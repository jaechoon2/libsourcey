//
// LibSourcey
// Copyright (C) 2005, Sourcey <http://sourcey.com>
//
// LibSourcey is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// LibSourcey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//


#ifndef SCY_Net_TCPBase_H
#define SCY_Net_TCPBase_H


#include "scy/uv/uvpp.h"
#include "scy/net/stream.h"
#include "scy/net/socket.h"
#include "scy/net/address.h"
#include "scy/net/types.h"


namespace scy {
namespace net {


} } // namespace scy::net


#endif // SCY_Net_TCPBase_H



	//virtual void* instance() { return this; }
	//bool _connected;