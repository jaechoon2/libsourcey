//
// LibSourcey
// Copyright (C) 2005, Sourcey <http://sourcey.com>
//
// LibSourcey is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// LibSourcey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//


#include "scy/media/videocapture.h"
#include "scy/media/mediafactory.h"
#include "scy/logger.h"
#include "scy/platform.h"
#include "scy/util.h"


#ifdef HAVE_OPENCV


namespace scy {
namespace av {

	
VideoCapture::VideoCapture(int deviceId) : 
	_base(MediaFactory::instance().getVideoCaptureBase(deviceId))
{
	TraceLS(this) << "Create: " << deviceId << std::endl;

	_base->addEmitter(&this->emitter);
	//_base->duplicate();
}


VideoCapture::VideoCapture(const std::string& filename)
{
	TraceLS(this) << "Create: " << filename << std::endl;

	// The file capture is owned by this instance
	_base = std::make_shared<VideoCaptureBase>(filename);
	_base->addEmitter(&this->emitter);

	//assert(_base->refCount() == 1);
}


VideoCapture::VideoCapture(std::shared_ptr<VideoCaptureBase> base) :  //VideoCaptureBase*
	_base(base) 
{
	TraceLS(this) << "Create: " << base << std::endl;
	_base->addEmitter(&this->emitter);
	//_base->duplicate();
}


VideoCapture::~VideoCapture() 
{
	TraceLS(this) << "Destroy: " << _base << std::endl;	// << ": " << _base->refCount()
	_base->removeEmitter(&this->emitter);
	//_base->release();
}


void VideoCapture::start() 
{
	TraceLS(this) << "Start" << std::endl;	
	emitter.enable(true);
}


void VideoCapture::stop() 
{
	TraceLS(this) << "Stop" << std::endl;
	emitter.enable(false);
}


void VideoCapture::getFrame(cv::Mat& frame, int width, int height)
{
	Mutex::ScopedLock lock(_mutex);	
	TraceLS(this) << "Get frame: " << width << "x" << height << std::endl;
	
	// Don't actually grab a frame here, just copy the current frame.
	cv::Mat lastFrame = _base->lastFrame();
	if ((width && lastFrame.cols != width) || 
		(height && lastFrame.rows != height)) {
		cv::resize(lastFrame, frame, cv::Size(width, height));
	}
	else {
		lastFrame.copyTo(frame);
	}
}

		
void VideoCapture::getEncoderFormat(Format& iformat) 
{
	iformat.name = "OpenCV";
	//iformat.id = "rawvideo";
	iformat.video.encoder = "rawvideo";
	iformat.video.pixelFmt = "bgr24";
	iformat.video.width = width();
	iformat.video.height = height();
	iformat.video.enabled = true;
}


int VideoCapture::width() 
{
	Mutex::ScopedLock lock(_mutex);
	return _base->width(); 
}


int VideoCapture::height() 
{
	Mutex::ScopedLock lock(_mutex);
	return _base->height(); 
}


bool VideoCapture::opened() const
{
	Mutex::ScopedLock lock(_mutex);
	return _base->opened();
}


bool VideoCapture::running() const 
{
	Mutex::ScopedLock lock(_mutex);
	return _base->running();
}


VideoCaptureBase& VideoCapture::base() 
{
	Mutex::ScopedLock lock(_mutex);
	return *_base;
}


//
// Video Capture Base
//


VideoCaptureBase::VideoCaptureBase(int deviceId) : 
	//SharedObject(true), // no longer deferred
	_deviceId(deviceId),
	_capturing(false),
	_opened(false),
	_stopping(false)
{
	TraceLS(this) << "Create: " << deviceId << std::endl;
	open();
	start();
}


VideoCaptureBase::VideoCaptureBase(const std::string& filename) : 
	//SharedObject(true), // no longer deferred
	_filename(filename),
	_deviceId(-1),
	_capturing(false),
	_opened(false),
	_stopping(false)
{
	TraceLS(this) << "Create: " << filename << std::endl;
	open();
	start();
}


VideoCaptureBase::~VideoCaptureBase() 
{	
	TraceLS(this) << "Destroy" << std::endl;
	assert(Thread::currentID() != _thread.tid());

	if (_thread.running()) {
		_stopping = true;

		// Because we are calling join on the thread this
		// destructor must never be called from within a
		// capture callback or we will result in deadlock.		
		_thread.join();
	}

	// Try to release the capture (automatic once unrefed)
	//try { release(); } catch (...) {}

	TraceLS(this) << "Destroy: OK" << std::endl;
}


void VideoCaptureBase::start() 
{
	TraceLS(this) << "Starting" << std::endl;
	{
		Mutex::ScopedLock lock(_mutex);

		if (!_thread.running()) {
			TraceLS(this) << "Initializing thread" << std::endl;
			if (!_opened)
				throw std::runtime_error("The capture must be opened before starting the thread.");

			_stopping = false;
			_counter.reset();	
			_thread.start(*this);
		}
	}
	while (!_capturing && error().empty()) {
		TraceLS(this) << "Starting: Waiting" << std::endl;
		scy::sleep(20);
	}

	TraceLS(this) << "Starting: OK" << std::endl;
}


void VideoCaptureBase::stop() 
{
	TraceLS(this) << "Stopping" << std::endl;
	assert(Thread::currentID() != _thread.tid());
	if (_thread.running()) {
		TraceLS(this) << "Terminating thread" << std::endl;		
		_stopping = true;
		_thread.join();
	}
}


bool VideoCaptureBase::open()
{
	TraceLS(this) << "Open" << std::endl;
	Mutex::ScopedLock lock(_mutex);
	assert(Thread::currentID() != _thread.tid());

	_opened = _capture.isOpened() ? true : 
		_filename.empty() ? 
			_capture.open(_deviceId) : 
			_capture.open(_filename);

	if (!_opened) {
		std::stringstream ss;
		ss << "Cannot open the video capture device: "; 
		_filename.empty() ? (ss << _deviceId) : (ss << _filename);
		throw std::runtime_error(ss.str());
	}

	return _opened;
}


void VideoCaptureBase::run() 
{
	try 
	{	
		// Grab an initial frame
		cv::Mat frame = grab();
		_capturing = true;
		bool empty = true;
		PacketSignal* next = nullptr;

		TraceLS(this) << "Running:"		
			<< "\n\tDevice ID: " << _deviceId
			<< "\n\tFilename: " << _filename
			<< "\n\tWidth: " << width() 
			<< "\n\tHeight: " << height() << std::endl;		

		while (!_stopping) {	
			{
				int size = 1; // dummy
				for (int idx = 0; idx < size; idx++) {
					Mutex::ScopedLock lock(_emitMutex); 
					size = _emitters.size(); 
					empty = size == 0;
					if (empty || idx >= size)
						break;
					next = _emitters[idx];
	
					TraceLS(this) << "Emitting: " << idx << ": " << _counter.fps << std::endl;
					MatrixPacket out(&frame);
					next->emit(next, out);
				}
			}
			
			// Update last frame less often while in limbo
			if (empty || !_stopping) {
				frame = grab();
				scy::sleep(50);
			}

			// Always call waitKey otherwise all hell breaks loose
			cv::waitKey(10);
		}	
	}
	catch (cv::Exception& exc) 
	{
		setError("OpenCV Error: " + exc.err);
		//assert(0);
	}
	catch (std::exception& exc) 
	{
		// The exception was not properly handled inside the signal
		// callback scope which represents a serious application error.
		// Time to get out the ol debugger!!!
		setError(exc.what());
		//assert(0);
	}
	
	TraceLS(this) << "Exiting" << std::endl;
	_capturing = false;
}


cv::Mat VideoCaptureBase::grab()
{	
	assert(Thread::currentID() == _thread.tid());

	Mutex::ScopedLock lock(_mutex);	

	// Grab a frame from the capture source
	_capture >> _frame;

	// If using file input and we reach eof the set
	// behavior is to loop the input video.
	if (!_filename.empty() && (!_frame.cols || !_frame.rows)) {
		_capture.release();
		_capture.open(_filename);
		if (!_capture.isOpened()) {
			assert(0 && "invalid frame");
			throw std::runtime_error("Cannot grab video frame: Cannot loop video source: " + name());
		}
		_capture >> _frame;
	}
		
	if (!_capture.isOpened())
		throw std::runtime_error("Cannot grab video frame: Device is closed: " + name());

	if (!_frame.cols || !_frame.rows)
		throw std::runtime_error("Cannot grab video frame: Got an invalid frame from device: " + name());

	_counter.tick();
	//_width = _frame.cols;
	//_height = _frame.rows;

	return _frame;
}
	

cv::Mat VideoCaptureBase::lastFrame() const
{
	Mutex::ScopedLock lock(_mutex);

	if (!_opened)
		throw std::runtime_error("Cannot grab video frame: Device is closed: " + (error().length() ? error() : 
			std::string("Check video device: " + name())));

	if (!_frame.cols && !_frame.rows)
		throw std::runtime_error("Cannot grab video frame: Device is closed: " + name());

	//assert(_capturing);
	if (_frame.size().area() <= 0)
		throw std::runtime_error("Cannot grab video frame: Invalid source frame: " + name());

	return _frame; // no data is copied
}


void VideoCaptureBase::addEmitter(PacketSignal* emitter)
{
	Mutex::ScopedLock lock(_emitMutex);	
	_emitters.push_back(emitter);	
}


void VideoCaptureBase::removeEmitter(PacketSignal* emitter)  
{	
	Mutex::ScopedLock lock(_emitMutex);	
	for (PacketSignalVec::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
		if (*it == emitter) {
			_emitters.erase(it);
			return;
		}
	}
	assert(0 && "unknown emitter");
}


void VideoCaptureBase::setError(const std::string& error)
{
	ErrorLS(this) << "Set error: " << error << std::endl;
	Mutex::ScopedLock lock(_mutex);	
	_error = error;
}


int VideoCaptureBase::width() 
{
	Mutex::ScopedLock lock(_mutex);	
	return int(_capture.get(CV_CAP_PROP_FRAME_WIDTH)); // not const
}


int VideoCaptureBase::height() 
{
	Mutex::ScopedLock lock(_mutex);	
	return int(_capture.get(CV_CAP_PROP_FRAME_HEIGHT)); // not const
}


bool VideoCaptureBase::opened() const
{
	Mutex::ScopedLock lock(_mutex);
	return _opened;
}


bool VideoCaptureBase::running() const 
{
	Mutex::ScopedLock lock(_mutex);
	return _thread.running();
}


int VideoCaptureBase::deviceId() const 
{
	Mutex::ScopedLock lock(_mutex);
	return _deviceId; 
}


std::string	VideoCaptureBase::filename() const 
{
	Mutex::ScopedLock lock(_mutex);
	return _filename; 
}


std::string VideoCaptureBase::name() const 
{
	Mutex::ScopedLock lock(_mutex);
	std::stringstream ss;
	_filename.empty() ? (ss << _deviceId) : (ss << _filename);
	return ss.str();
}


std::string VideoCaptureBase::error() const 
{
	Mutex::ScopedLock lock(_mutex);
	return _error;
}


double VideoCaptureBase::fps() const
{
	Mutex::ScopedLock lock(_mutex);
	return _counter.fps; 
}


cv::VideoCapture& VideoCaptureBase::capture()
{
	Mutex::ScopedLock lock(_mutex);
	return _capture; 
}


} } // namespace scy::av


#endif

				/*
					//empty = !!size;
				//while (idx < size) {
					//idx++;
				//Mutex::ScopedLock lock(_emitMutex); 
				if (!_emitters.empty()) {
					frame = grab();
					MatrixPacket packet(&frame);
			
					_insideCallback = true;
					for (unsigned idx = 0; i < _emitters.size(); i++) {
						TraceLS(this) << "Emitting: " << _counter.fps << std::endl;
						_emitters[i]->emit(_emitters[i], packet);
					}
					_insideCallback = false;
					continue;
				}
				*/


/*
void VideoCaptureBase::release()
{
	TraceLS(this) << "Release" << std::endl;
	Mutex::ScopedLock lock(_mutex);
	if (_capture.opened())
		_capture.release();
	_opened = false;
	TraceLS(this) << "Release: OK" << std::endl;
}
*/

			/*
			// Grab a frame if we have delegates or aren't syncing with them
			hasDelegates = emitter().refCount() > 0;
			if (hasDelegates || (!hasDelegates && !syncWithDelegates)) {
				frame = grab();
				MatrixPacket packet(&frame);
				if (hasDelegates && packet.width && packet.height && !_stopping) {
					TraceLS(this) << "Emitting: " << _counter.fps << std::endl;
					emit(packet);
				}				
				continue;
			}
			*/


/*
void VideoCaptureBase::attach(const PacketDelegateBase& delegate)
{
	PacketSignal::attach(delegate);
	TraceLS(this) << "Added Delegate: " << refCount() << std::endl;
	if (!running() && flags().has(SyncWithDelegates)) //refCount == 1
		start();
}


bool VideoCaptureBase::detach(const PacketDelegateBase& delegate) 
{
	if (PacketSignal::detach(delegate)) {
		TraceLS(this) << "Removed Delegate: " << refCount() << std::endl;
		if (refCount() == 0 && flags().has(SyncWithDelegates)) //running() && 
			stop();
		return true;
	}
	return false;
}
*/


/*
Flaggable VideoCaptureBase::flags() const
{
	Mutex::ScopedLock lock(_mutex);
	return _flags;
}
*/


/*
bool VideoCaptureBase::check()
{	
	grab();	

	Mutex::ScopedLock lock(_mutex);	

	// TODO: Check COM is multithreaded when using dshow highgui.

	// Ensure the captured frame is not empty.
	if (!frame.cols ||!frame.rows ) {
		std::stringstream ss;
		ss << "Cannot open video capture, please check device: "; 
		_filename.empty() ? (ss << _deviceId) : (ss << _filename);
		throw std::runtime_error(ss.str());
	}

	return true;
}
*/