Please see http://sourcey.com/projects/libsourcey


EXTERNAL DEPENDENCIES
=================================================

OpennSSL >= 0.98
-------------------------------------------------
tar zxvf openssl-1.0.1e.tar.gz
cd openssl-1.0.1e
./configure mingw --prefix=/usr/local shared
make
make install


Poco >= 1.4
-------------------------------------------------
tar zxvf poco-1.4.6p1-all.tar.gz
cd poco-1.4.6p1-all		
./configure --shared --no-tests --no-samples --omit=XML,Data,Data/SQLite,Data/MySQL,Data/ODBC,PageCompiler,PageCompiler/File2Page
make
make install


OpenCV >= 2.3
-------------------------------------------------



FFmpeg >= 6
-------------------------------------------------


INTERNAL DEPENDENCIES
=================================================
LibSourcey also has a number of dependencies which we build ourselves.
Remember that dependencies must be built before the main project. 

PugiXML
RtAudio
libstrophe


OBTAINING SOURCES
=================================================
git clone https://bitbucket.org/sourcey/libsourcey.git



BUILDING
=================================================

Install CMake


Open CMake


Linux
-------------------------------------------------

1) Buld Dependencies

make install


2) Buld Modules

make install


3) Build Projects

make install